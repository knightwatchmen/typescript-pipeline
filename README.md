# README #

#### Quick summary ####
This is a data pipeline framework that runs in NodeJS.  The pipeline is composed of processing blocks 
which have a deifined input contract and a defined output contract.  All the block needs to do is 
handle when an input contract is passed in, and needs to emit the output contract.  The routing, queuing, 
error capture, and pushing to the next processing block are handled for you.

### Additional Info ###
For additional info, please see the wiki/homepage at: 
[https://bitbucket.org/pythagorasio/typescript-pipeline/wiki/Home](https://bitbucket.org/pythagorasio/typescript-pipeline/wiki/Home)

### Bugs and Issues
Please post any bugs or issues to the issues site 
at [https://bitbucket.org/pythagorasio/typescript-pipeline/issues](https://bitbucket.org/pythagorasio/typescript-pipeline/issues)