import {Mortar, MortarOptions, MortarPlugin} from "./Mortar";
import {Block} from "./Block";

export class Pipeline {

    private blockCount: number = 0;
    private graph: { [key: string]: BlockGraphNode } = {};
    private mortars: Mortar[] = [];

    constructor() {
    }

    public registerBlock(block: Block): string {
        // Make sure block is not already registered
        if (block.getKey() !== undefined) throw new Error('Can\'t register the same block twice.');

        // Set key in block
        this.blockCount++;
        let key = (this.blockCount).toString();
        block.setKey(key);

        // Store in list
        this.graph[key] = new BlockGraphNode(block);

        return key;
    }

    public bindBlocks<A extends MortarPlugin>(current: Block, next: Block, mortarOptions?:MortarOptions, plugin?: A): boolean {

        // Get BGNs
        let _curr = this.getBGN(current);
        let _next = this.getBGN(next);

        // Make sure not overwriting next.
        let t = _curr.getNext();
        if (!(typeof t === 'undefined')) throw new Error('Cannot bind next when next is already set.');

        // Make sure the contracts are compatible,
        let output = current.getOutputContract();
        let input = next.getInputContract();

        if (!(output && input)) throw new Error('Must define contract for block.');

        let outputObj = new output();
        if (!(outputObj instanceof input)) {
            throw new Error('Contract mismatch.');
        }

        // Setup the mortar to glue the blocks together
        let ps = new Mortar(mortarOptions);
        ps.initialize(current, next, plugin,);
        this.mortars.push(ps);

        // Set the node links.
        _curr.setNext(_next);
        _next.addPrev(_curr);
        return true;
    }

    public getBlock(blockKey: string): Block {
        if (!(typeof blockKey === 'string' && blockKey in this.graph)) throw new Error('Block must be registered first.');
        return this.graph[blockKey].getBlock();
    }

    public getNextBlock(current: Block): Block {
        let _curr = this.getBGN(current);
        let next = _curr.getNext();
        if (!(next instanceof BlockGraphNode)) throw new Error('Next Block not defined.');
        return next.getBlock();
    }

    public hasNextBlock(current: Block): boolean {
        let _curr = this.getBGN(current);
        let next = _curr.getNext();
        return (next instanceof BlockGraphNode);
    }

    private getBGN(block: Block): BlockGraphNode {
        let key = block.getKey();
        if (!(typeof key === 'string' && key in this.graph)) throw new Error('Block must be registered first.');
        return this.graph[key];
    }

    public pause(): void {
        for (let x in this.mortars) {
            this.mortars[x].pause();
        }
    }

    public resume(): void {
        for (let x in this.mortars) {
            this.mortars[x].resume();
        }
    }
}



class BlockGraphNode {
    private next: BlockGraphNode;
    private prev: BlockGraphNode[] = [];
    private block: Block;

    constructor(block: Block) {
        this.block = block;
    }

    public setNext(next: BlockGraphNode): BlockGraphNode {
        this.next = next;
        return this;
    }

    public addPrev(prev: BlockGraphNode): BlockGraphNode {
        this.prev.push(prev);
        return this;
    }

    public getNext(): BlockGraphNode {
        return this.next;
    }

    public getBlock(): Block {
        return this.block;
    }

}
