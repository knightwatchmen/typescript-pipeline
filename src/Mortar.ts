import {Contract} from "./Contract";
import {EventEmitter} from "events";
import {Block} from "./Block";

export interface MortarOptions {
    logContractsToConsole: boolean;
}

export class Mortar {

    private inputBlock: Block;
    private outputBlock: Block;
    private plugin: MortarPlugin;
    private options: MortarOptions;

    constructor(options: MortarOptions = <MortarOptions>{}) {
        this.options = Object.assign(<MortarOptions>{
            logContractsToConsole: false,
        }, options);
    }

    public initialize<A extends MortarPlugin>(inputBlock: Block, outputBlock: Block, plugin?: A): void {
        this.inputBlock = inputBlock;
        this.outputBlock = outputBlock;

        if (plugin) {
            // Validate the plugin
            if (!(plugin === undefined || plugin instanceof MortarPlugin)) throw new Error('Invalid plugin.');
            this.plugin = plugin;

            // Wire up the plugin
            inputBlock.on('data', chunk => {
                this.checkContract(chunk);
                plugin.write(chunk);
            });
            plugin.on('data', (response: any) => {
                this.onPluginData(response);
            });
        } else {
            inputBlock.on('data', this.send);
        }

        if (this.options.logContractsToConsole === true) {
            inputBlock.on('data', chunk => {
                console.log('Contract: ' + chunk);
            });
        }
    }

    public resume(): void {
        if (this.plugin) {
            this.plugin.resume();
        }
    }

    public pause(): void {
        if (this.plugin) {
            this.plugin.pause();
        }
    }

    private onPluginData(response: MortarPluginDataEvent): void {
        let outputContract = this.inputBlock.getOutputContract();
        let contract = Object.assign(new outputContract(), response.contract);
        this.send(contract);
    }

    private send(contract: Contract): void {
        this.checkContract(contract);
        this.outputBlock.handle(contract).then(result => {
            //TODO: Do we need to do anything here?
        }).catch(err => {
            //TODO: Handle errors from processing blocks
        });
    }

    private checkContract(chunk: any) {
        if (chunk === undefined) return;
        let outputContract = this.inputBlock.getOutputContract();
        if (!(chunk instanceof outputContract)) {
            throw new Error('Invalid output contract.');
        }
        let obj = <Contract>chunk;
        if (!(obj.validateStructure())) throw new Error('Invalid output contract structure.');
    }
}

export abstract class MortarPlugin extends EventEmitter {

    constructor() {
        super();
    }

    abstract write<A extends Contract>(contract: A): Promise<boolean>;

    abstract resume(): void;

    abstract pause(): void;
}

export interface MortarPluginDataEvent {
    contract: object;
    callback: Function;
}