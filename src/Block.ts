import {EventEmitter} from "events";
import {Contract} from "./Contract";

export abstract class Block extends EventEmitter{
    private key: string;
    private inputContract: new() => any;
    private outputContract: new() => any;

    constructor() {
        super();
    }

    public getInputContract<A extends Contract>(): new() => A {
        return this.inputContract;
    }

    public getOutputContract<A extends Contract>(): new() => A {
        return this.outputContract;
    }

    protected setInputContract<A extends Contract>(inputContract: new() => A): void {
        this.inputContract = inputContract;
    }

    protected setOutputContract<A extends Contract>(outputContract: new() => A): void {
        this.outputContract = outputContract;
    }

    public setKey(key: string): void {
        this.key = key;
    }

    public getKey(): string {
        return this.key;
    }

    abstract handle<A extends Contract>(input: A): Promise<boolean>;
}