/**
 * Created by wpigott on 6/29/17.
 */
import { EventEmitter } from "events";
export declare class Pipeline {
    private blockCount;
    private graph;
    private mortars;
    constructor();
    registerBlock(block: Block): string;
    bindBlocks<A extends MortarPlugin>(current: Block, next: Block, mortarOptions?: MortarOptions, plugin?: A): boolean;
    getBlock(blockKey: string): Block;
    getNextBlock(current: Block): Block;
    hasNextBlock(current: Block): boolean;
    private getBGN(block);
    pause(): void;
    resume(): void;
}
export interface MortarOptions {
    logContractsToConsole: boolean;
}
export declare class Mortar {
    private inputBlock;
    private outputBlock;
    private plugin;
    private options;
    constructor(options?: MortarOptions);
    initialize<A extends MortarPlugin>(inputBlock: Block, outputBlock: Block, plugin?: A): void;
    resume(): void;
    pause(): void;
    private onPluginData(response);
    private send(contract);
    private checkContract(chunk);
}
export declare abstract class MortarPlugin extends EventEmitter {
    constructor();
    abstract write<A extends Contract>(contract: A): Promise<boolean>;
    abstract resume(): void;
    abstract pause(): void;
}
export interface MortarPluginDataEvent {
    contract: object;
    callback: Function;
}
export declare abstract class Contract {
    abstract validateStructure(): boolean;
}
export declare abstract class Block extends EventEmitter {
    private key;
    private inputContract;
    private outputContract;
    constructor();
    getInputContract<A extends Contract>(): new () => A;
    getOutputContract<A extends Contract>(): new () => A;
    protected setInputContract<A extends Contract>(inputContract: new () => A): void;
    protected setOutputContract<A extends Contract>(outputContract: new () => A): void;
    setKey(key: string): void;
    getKey(): string;
    abstract handle<A extends Contract>(input: A): Promise<boolean>;
}
